#include <iostream>
#include <time.h>

using namespace std;



int main()
{

	const int M = 7;
	const int N = 5;

	int A[M][N];
	for (int i = 0; i < M; i++)
	{
		for (int j = 0; j < N; j++)
		{
			A[i][j] = i + j;
			cout << A[i][j] << ' ';
		}
		cout << endl;
	}
	cout << endl;

	struct tm buf;
	time_t t = time(NULL);
	localtime_s(&buf, &t);
	int day = buf.tm_mday;
	int sum = 0;

	for (int x = 0; x < N; x++)
	{
		sum += A[day % N][x];
	}

	cout << sum << endl;
	return 0;
}
